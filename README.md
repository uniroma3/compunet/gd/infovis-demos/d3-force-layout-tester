# Project information

This is a demo project for the "Information Visualization" course at the Roma Tre University.
It allows you to play with the parameters of the D3.js force layout algorithm.

The project can be accessed at: https://uniroma3.gitlab.io/compunet/gd/infovis-demos/d3-force-layout-tester/.


![The project interface](https://uniroma3.gitlab.io/compunet/gd/infovis-demos/d3-force-layout-tester/images/d3-tester-screenshot.jpg)

## Credits

Thanks to Matteo Bellatreccia, who realized this demo as part of his Bachelor degree final project.

## Todos

- Update the project with a more recent version of D3.js
- Allow the user to choose different kinds of default datasets
- Allow the user to save a dataset to the client harddisk

